<?php

// bldgcrawler.php
// Loops through sequential get requests to the OSU Campus Map and retrieves 
// any additional landmark info as JSON reply.  Parse JSON reply and output
// to browser.

// Vaughan Schmidt
// schmicar@onid.engr.oregonstate.edu
// 2014.11.13

header('Content-Type: application/json');
$capture = array();

for($i=447; $i < 1039 ; $i++) {
  $url = 'http://oregonstate.edu/campusmap/map/element?location=&locations=' . $i;
  $json = file_get_contents($url);
  
  $obj=json_decode($json, true);

  $name = $obj["locations"][$i]["name"];
  $abbr = $obj["locations"][$i]["abbrev"];
  $id = $obj["locations"][$i]["id"];
  $lat = $obj["locations"][$i]["marker_location"][1];
  $long = $obj["locations"][$i]["marker_location"][0];
  $addr1 = $obj["locations"][$i]["metadata"]["physical_address"];
  $notes = $obj["locations"][$i]["metadata"]["long_description"];

  if((int)$id > 0) {
    echo("(" . (int)$id . ",\"$abbr\",\"$name\",\"$addr1\",NULL,\"Corvallis\",\"OR\",\"97333\"," . (float)$lat . "," . (float)$long . ",\"$notes\",NOW(),NOW()),");
    echo "
  ";
  }
  
  // builds an array - not needed for How To, but for future use.
  $element = array(
    "id" => $id,
    "abbr" => $abbr,
    "name" => $name,
    "lat" => (float)$lat,
    "long" => (float)$long,
    "addr" => $addr,
    "city" => "Corvallis",
    "st" => "OR",
    "zip" => "97333",
    "notes" => $notes);
  array_push($capture, $element);
};

die();
?>