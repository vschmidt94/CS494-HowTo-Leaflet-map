

var map1 = L.map('map1').setView([44.55957, -123.28141355887], 13);

L.tileLayer('http://{s}.tiles.mapbox.com/v3/schmicar.k7apgihh/{z}/{x}/{y}.png', 
{
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18
}).addTo(map1);