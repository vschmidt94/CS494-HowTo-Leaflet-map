
var map2 = L.map('map2').setView([44.56357, -123.28141355887], 14);

L.tileLayer('http://{s}.tiles.mapbox.com/v3/schmicar.k7apgihh/{z}/{x}/{y}.png', 
{
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18
}).addTo(map2);

var points = [
  {
    'name': 'Valley Library',
    'lat': 44.5650618928,
    'lng': -123.27603917
  },
  {
    'name': 'Cascade Hall',
    'lat': 44.5615013978,
    'lng': -123.275598074
  },
  {
    'name': 'Reser Stadium',
    'lat': 44.559575735338,
    'lng': -123.28141355887
  }
];

var markers = new Array();

for (i = 0; i < points.length; i++) {
  markers[i] = L.marker([points[i].lat, points[i].lng]).addTo(map2);
  markers[i].bindPopup(points[i].name);
}

