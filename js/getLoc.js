var myMarker = null;
var myLL;
var lat = 0;
var lng = 0;
var closeet = null;
var closestIdx = null;
var closestDist = 0;


// from: http://www.w3schools.com/html/html5_geolocation.asp
function getLocation() {
  var x = document.getElementById('getLoc');
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = 'Geolocation is not supported by this browser.';
  }
}

function showPosition(position) {
  var x = document.getElementById('getLoc');
  lat = position.coords.latitude;
  lng = position.coords.longitude;
  var map3 = L.map('map3').setView([lat, lng], 13);
  map3.touchZoom = false;
  map3.scrollWheelZoom = false;
  x.innerHTML = 'USER LOCATION<br>Latitude: ' + 
    position.coords.latitude + '<br>Longitude: ' + position.coords.longitude;
  L.tileLayer('http://{s}.tiles.mapbox.com/v3/schmicar.k7apgihh/{z}/{x}/{y}.png',
  {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18
  }).addTo(map3);

  var destIcon = L.MakiMarkers.icon({icon: 'marker', color: '#03c'})
  for (i = 0; i < points.length; i++) {
    markers[i] = L.marker([points[i].lat, points[i].lng], {icon: destIcon}).addTo(map3);
    markers[i].bindPopup(points[i].name);
  }

  x.className = 'highlighted';

  // make a new marker for user, and give it a custom icon
  var icon = L.MakiMarkers.icon({icon: 'rocket', color: '#b0b', size: 'm'});
  myMarker = L.marker([lat, lng], {icon: icon}).addTo(map3);
  myLL = L.latLng(lat, lng);

  // add new marker to array of markers - will need to be grouped to set bounds
  markers.push(myMarker);

  // set up a marker group to be able to set map bounds to hold all markers
  var group = new L.featureGroup(markers);

  // now scale the map to fit in all the markers.  The pad() method will prevent
  // tops of north-most markers from getting cut off.
  map3.fitBounds(group.getBounds().pad(0.5));

  // while we're at it, find the closest marker to user
  closest = L.GeometryUtil.closest(map3, points, myLL);

  for (i = 0; i < points.length; i++) {
    var delta = 1E-14;
    var latDiff = Math.abs(points[i].lat - closest.lat);
    var lngDiff = Math.abs(points[i].lng - closest.lng);
    if (((latDiff < delta) && (lngDiff < delta))) {
      closestIdx = i;
      closestDist = myLL.distanceTo(points[i]);
      break;   // if 2 markers are identical-enough, only return 1st
    }
  }

  // output results of nearest marker
  x = document.getElementById('nearestMarker');
  x.className = 'highlighted';
  x.innerHTML = 'NEAREST MARKER<br>Name: ' + points[closestIdx].name +
    '<br>Latitude: ' + points[closestIdx].lat +
    '<br>Longitude: ' + points[closestIdx].lng +
    '<br>Distance: ' + (Math.round(10 * (closestDist / 1000)) / 10) +
    ' km / ' + (Math.round(10 * (closestDist / 1609.34)) / 10) + ' mi';

}



